



export let infoChange = (viTri) => {
  return `<div style="color : white"><p> ${viTri.name} - ${viTri.brand} (${viTri.color})</p> 
    <span style="padding : 10px 5px; background : red">${viTri.price} $</span>
    <p style=" margin: 20px 0px">${viTri.description}</p></div>
    `;
};

export let imageChange = (viTri) => {
  return `<img src="${viTri.virtualImg}" alt="" />`;
};
